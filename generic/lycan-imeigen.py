#!/usr/bin/env python

# Valid IMEI generator by given TAC/prefix

# Created by Luxferre in 2021
# Released into public domain

from random import randint

# define the constants

VERSION = '0.1'

def luhn(imeistr):
    evenmap = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
    digits = [int(d) for d in imeistr]
    oddSum = sum(digits[0::2])
    evenSum = sum([evenmap[d] for d in digits[1::2]])
    rem = (oddSum + evenSum) % 10
    return (10 - rem) % 10

def imeigen(prefix):
    bimei = prefix[:14]
    plen = len(bimei)
    if plen < 14:
        rlen = 14 - plen
        rlist = [str(randint(0, 9)) for i in range(0, rlen)]
        bimei += ''.join(rlist)
    return bimei + str(luhn(bimei))

# main code

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Lycan IMEIGEN v%s: valid IMEI generator by given TAC/prefix' % VERSION, epilog='(c) Luxferre 2021 --- No rights reserved <https://unlicense.org>')
    parser.add_argument('prefix', help='IMEI prefix to generate from')
    parser.add_argument('-v','--version', action='version', version='Lycan IMEIGEN %s' % VERSION, help='Show version and exit')
    args = parser.parse_args()
    print(imeigen(args.prefix))
